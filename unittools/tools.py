#!/usr/bin/env python
import sys
import os

class TestType(object):
    RETURNS = 'returns'
    FAILS = 'fails'

class TestSuite(object):
    ''' Suite that remembers which unit tests to run. '''

    def __init__(self, env_activate=None, env_deactivate=None):
        self.activate = env_activate
        self.deactivate = env_deactivate
        self.tests = []

    def active(self):
        ''' If env_activate var is set, then only run if env var exists.
        If env_deactivate var is set, run always except when env var exists.
        '''
        if self.deactivate and os.getenv(self.deactivate):
            # deactivate is set, and found in env
            return False
        if self.activate:
            # activate is set, so return whether env var exists
            return os.getenv(self.activate)
        # default is just to run
        return True
                

    def returns(self, output=None, args=None, kwargs=None):
        def returns_deco(func):
            self.tests += [(TestType.RETURNS, func, (output, args, kwargs))]
            return func
        return returns_deco

    def fails(self, exception=None, args=None, kwargs=None):
        def fails_deco(func):
            self.tests += [(TestType.FAILS, func, (exception, args, kwargs))]
            return func
        return fails_deco

    def param_string(self, args, kwargs):
        param_str = ''
        if args:
            param_str = ','.join((str(x) for x in args))
            if kwargs:
                param_str += ', '
        if kwargs:
            param_str += ','.join(('{}={}'.format(k, kwargs[k]) for k in kwargs))
        return param_str

    def call_string(self, name, args, kwargs):
        return '{}({})'.format(name, self.param_string(args, kwargs))

    def test_returns(self, func, test_args):
        output, args, kwargs = test_args
        sys.stdout.write('{} returns {}... '.format(
            self.call_string(func.func_name, args, kwargs),
            output,
        ))
        actual_output = func(*(args or []), **(kwargs or {}))
        return actual_output == output

    def test_fails(self, func, test_args):
        exception, args, kwargs = test_args
        sys.stdout.write('{} fails with {}... '.format(
            self.call_string(func.func_name, args, kwargs),
            exception.__name__,
        ))
        try:
            func(*(args or []), **(kwargs or {}))
        except exception:
            return True
        else:
            return False

    def test(self, type, func, test_args):
        test_function = getattr(self, 'test_{}'.format(type))
        if test_function(func, test_args):
            sys.stdout.write('PASS\n')
            return 1
        sys.stdout.write('FAIL\n')
        return 0

    def run(self):
        if not self.active():
            return
        tests_passed = 0
        for test_type, func, test_args in self.tests:
            tests_passed += self.test(test_type, func, test_args)
        print('Tests passed: {}/{}'.format(tests_passed, len(self.tests)))
