import tools

suite = tools.TestSuite(env_activate='DEBUG')

@suite.returns(10, (4, 6))
@suite.returns(15, (10, 5))
@suite.returns(20, (10, 10))
def addem(x, y):
    return x + y

@suite.returns("xy")
def wontpass():
    return "x"

@suite.returns('foobar', kwargs={'foo': 'foo', 'bar': 'bar'})
def appender(foo='xxx', bar='yyy'):
    return '{}{}'.format(foo, bar)

@suite.fails(ValueError, ('{dfds}',))
@suite.fails(ValueError, ('{"hi": 1}',))
def jsontest(s):
    import json
    return json.loads(s)

suite.run()
